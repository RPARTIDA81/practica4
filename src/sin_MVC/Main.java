/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package sin_MVC;

import java.util.Scanner;

/**
 * @author Rafael Partida Ibáñez (1516Ceed72)
 * @email rpartida81@yahoo.es
 * @fecha de creación 25-nov-2015
 */
public class Main {
    
    
    
    public static void main (String[] args){
    String id_alumno = null;
    String id_grupo = null;
    String nombre_alumno = null;
    String nombre_grupo = null;
    int opcion = 1;
    
        do {
    System.out.println("** MENU **");
    System.out.println("PRACTICA CLASE");
    System.out.println("0. Salir");
    System.out.println("1. Grupo");
    System.out.println("2. Alumno");
    System.out.print("Opción: ");
    Scanner scn = new Scanner (System.in);
    opcion = scn.nextInt();
    switch (opcion){
        case 1:
            System.out.println("OBTENER GRUPO");
            System.out.print("Id: ");
            Scanner grupo = new Scanner (System.in);
            id_grupo = grupo.nextLine();
            System.out.print("Nombre: ");
            nombre_grupo = grupo.nextLine();
            break;
        case 2:
            System.out.println("OBTENER ALUMNO");
            System.out.print("Id: ");
            Scanner alumno = new Scanner (System.in);
            id_alumno = alumno.nextLine();
            System.out.print("Nombre: ");
            nombre_alumno = alumno.nextLine();
            System.out.println("************");
            System.out.println("MOSTRAR ALUMNO");
            System.out.println("************");
            System.out.println("Id: "+ id_alumno);
            System.out.println("Nombre: "+ nombre_alumno);
            System.out.println("El Alumno pertenece al siguiente grupo:");
            System.out.println("************");
            System.out.println("MOSTRAR GRUPO");
            System.out.println("************");
            System.out.println("Id: "+ id_grupo);
            System.out.println("Nombre: "+ nombre_grupo);
            break;
        case 0:
            opcion = 0;
            break;
            
    }
}
while(opcion !=0);
    
    }
}
