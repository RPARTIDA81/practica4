/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package vista;

import java.util.Scanner;
import modelo.Alumno;
import modelo.Grupo;

/**
 * @author Rafael Partida Ibáñez (1516Ceed72)
 * @email rpartida81@yahoo.es
 * @fecha de creación 23-nov-2015
 */
public class VistaAlumno implements IVista<Alumno> {
    private String nombre;
    private String id;  
    
  
    public void mostrar(Alumno alumno){
        System.out.println("************P");
        System.out.println("MOSTRAR ALUMNO.");
        System.out.println("*************");
        System.out.println("Id: " + alumno.getId());
        System.out.println("Nombre: " + alumno.getNombre());
        System.out.println("El alumno pertenece al siguiente grupo:");
        VistaGrupo vistag = new VistaGrupo();
        Grupo grupoa = alumno.getGrupo();
        vistag.mostrar(grupoa);
        
    }
     
    public Alumno obtener(){
            Alumno alumno = new Alumno();
            Scanner scn = new Scanner (System.in);
            System.out.println("OBTENER ALUMNO");
            System.out.print("Id: ");
            id = scn.nextLine();
            alumno.setId(id);
            System.out.print("Nombre: ");
            nombre = scn.nextLine();
            alumno.setNombre(nombre);
            
            return alumno;
     }

}
