/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controlador;

import modelo.*;
import vista.*;

/**
 * @author Rafael Partida Ibáñez (1516Ceed72)
 * @email rpartida81@yahoo.es
 * @fecha de creación 23-nov-2015
 */
public class Controlador {    
    
    private Grupo grupoc = new Grupo();
    private IVista<Alumno> vista_ac = new VistaAlumno();
    private IVista<Grupo> vista_gc = new VistaGrupo();
    private Boolean mientras = true;
    
    
    public Controlador (Alumno alumno, Vista vista){
                
        Configurador.getConfigurador();
        Configurador.getConfigurador();
        do{ 
           int opcion = vista.menu();
           
           switch(opcion){
                    case 1:
                    grupoc = vista_gc.obtener();
                    break;
                    
                    case 2:
                    alumno = vista_ac.obtener();
                    alumno.setGrupo(grupoc);
                    vista_ac.mostrar(alumno);
                    break;
                        
                    case 0:
                    mientras = false;
                    break;
           }   
        }while(mientras);
           
        }
    }